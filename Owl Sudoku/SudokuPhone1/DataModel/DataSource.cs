﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;
using Windows.Storage;

namespace SudokuPhone1.DataModel
{
    public class Result
    {
        public TimeSpan time { get; set; }
        public string name { get; set; }
        public DateTime winTime { get; set; }
        public Result()
        {
            time = new TimeSpan();
            name = "Player 1";
            winTime = new DateTime();
        }
    }

    public class DataSource
    {
        private ObservableCollection<Result> resultsExtEasy;
        private ObservableCollection<Result> resultsEasy;
        private ObservableCollection<Result> resultsMedium;
        private ObservableCollection<Result> resultsDifficult;
        private ObservableCollection<Result> resultsEvil;

        private const string extEasy = "ExtEasy";
        private const string easy = "Easy";
        private const string medium = "Medium";
        private const string difficult = "Difficult";
        private const string evil = "Evil";

        private const string extEasyResult = "ExtEasyResults";
        private const string easyResult = "EasyResults";
        private const string mediumResult = "MediumResults";
        private const string difficultResult = "DifficultResults";
        private const string evilResult = "EvilResults";

        public DataSource()
        {
            resultsExtEasy = new ObservableCollection<Result>();
            resultsEasy = new ObservableCollection<Result>();
            resultsMedium = new ObservableCollection<Result>();
            resultsDifficult = new ObservableCollection<Result>();
            resultsEvil = new ObservableCollection<Result>();
        }

        #region Extremely Easy Block
        public async Task<ObservableCollection<Result>> GetExtEasyResultAsync()
        {
            await ensureDataLoaded(extEasy);
            return resultsExtEasy;
        }

        private async Task getExtEasyResultAsync()
        {
            if (resultsExtEasy.Count != 0)
                return;

            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));

            try
            {
                using(var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(extEasyResult))
                {
                    resultsExtEasy = (ObservableCollection<Result>)jsonSerializer.ReadObject(stream);
                }
            }
            catch
            {
                resultsExtEasy = new ObservableCollection<Result>();
            }
        }

        private async Task AddExtEasyResultAsync(Result result)
        {
            resultsExtEasy.Add(result);
            IEnumerable<Result> ordered = resultsExtEasy.OrderBy(res => res.time);
            resultsExtEasy = (ObservableCollection<Result>)ordered.Take(10);
            await saveDataAsync(extEasy);
        }
        #endregion
        #region Easy Block
        public async Task<ObservableCollection<Result>> GetEasyResultAsync()
        {
            await ensureDataLoaded(easy);
            return resultsEasy;
        }

        private async Task getEasyResultAsync()
        {
            if (resultsEasy.Count != 0)
                return;

            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));

            try
            {
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(easyResult))
                {
                    resultsEasy = (ObservableCollection<Result>)jsonSerializer.ReadObject(stream);
                }
            }
            catch
            {
                resultsEasy = new ObservableCollection<Result>();
            }
        }

        private async Task AddEasyResultAsync(Result result)
        {
            resultsEasy.Add(result);
            IEnumerable<Result> ordered = resultsEasy.OrderBy(res => res.time);
            resultsEasy = (ObservableCollection<Result>)ordered.Take(10);
            await saveDataAsync(easy);
        }
        #endregion
        #region Medium Block
        public async Task<ObservableCollection<Result>> GetMediumResultAsync()
        {
            await ensureDataLoaded(medium);
            return resultsMedium;
        }

        private async Task getMediumResultAsync()
        {
            if (resultsMedium.Count != 0)
                return;

            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));

            try
            {
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(mediumResult))
                {
                    resultsMedium = (ObservableCollection<Result>)jsonSerializer.ReadObject(stream);
                }
            }
            catch
            {
                resultsMedium = new ObservableCollection<Result>();
            }
        }

        private async Task AddMediumResultAsync(Result result)
        {
            resultsMedium.Add(result);
            IEnumerable<Result> ordered = resultsMedium.OrderBy(res => res.time);
            resultsMedium = (ObservableCollection<Result>)ordered.Take(10);
            await saveDataAsync(medium);
        }
        #endregion
        #region Difficult Block
        public async Task<ObservableCollection<Result>> GetDifficultResultAsync()
        {
            await ensureDataLoaded(difficult);
            return resultsDifficult;
        }

        private async Task getDifficultResultAsync()
        {
            if (resultsDifficult.Count != 0)
                return;

            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));

            try
            {
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(difficultResult))
                {
                    resultsDifficult = (ObservableCollection<Result>)jsonSerializer.ReadObject(stream);
                }
            }
            catch
            {
                resultsDifficult = new ObservableCollection<Result>();
            }
        }

        private async Task AddDifficultResultAsync(Result result)
        {
            resultsEasy.Add(result);
            IEnumerable<Result> ordered = resultsDifficult.OrderBy(res => res.time);
            resultsDifficult = (ObservableCollection<Result>)ordered.Take(10);
            await saveDataAsync(difficult);
        }
        #endregion
        #region Evil Block
        public async Task<ObservableCollection<Result>> GetEvilResultAsync()
        {
            await ensureDataLoaded(evil);
            return resultsEvil;
        }

        private async Task getEvilResultAsync()
        {
            if (resultsEvil.Count != 0)
                return;

            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));

            try
            {
                using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(evilResult))
                {
                    resultsEvil = (ObservableCollection<Result>)jsonSerializer.ReadObject(stream);
                }
            }
            catch
            {
                resultsEvil = new ObservableCollection<Result>();
            }
        }

        private async Task AddEvilResultAsync(Result result)
        {
            resultsEasy.Add(result);
            IEnumerable<Result> ordered = resultsEvil.OrderBy(res => res.time);
            resultsEvil = (ObservableCollection<Result>)ordered.Take(10);
            await saveDataAsync(evil);
        }
        #endregion
        private async Task ensureDataLoaded(string level)
        {
            switch (level)
            {
                case "ExtEasy":
                    if (resultsExtEasy.Count == 0)
                        await getExtEasyResultAsync();
                    break;
                case "Easy":
                    if (resultsEasy.Count == 0)
                        await getEasyResultAsync();
                    break;
                case "Medium":
                    if (resultsMedium.Count == 0)
                        await getMediumResultAsync();
                    break;
                case "Difficult":
                    if (resultsDifficult.Count == 0)
                        await getExtEasyResultAsync();
                    break;
                case "Evil":
                    if (resultsEvil.Count == 0)
                        await getEvilResultAsync();
                    break;
                default:
                    break;
            }
        }
        private async Task saveDataAsync(string level)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Result>));
            switch (level)
            {
                case extEasy:
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(extEasyResult, 
                        CreationCollisionOption.ReplaceExisting))
                    {
                        jsonSerializer.WriteObject(stream, resultsExtEasy);
                    }
                    break;
                case easy:
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(easyResult,
                        CreationCollisionOption.ReplaceExisting))
                    {
                        jsonSerializer.WriteObject(stream, resultsEasy);
                    }
                    break;
                case medium:
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(mediumResult,
                        CreationCollisionOption.ReplaceExisting))
                    {
                        jsonSerializer.WriteObject(stream, resultsMedium);
                    }
                    break;
                case difficult:
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(difficultResult,
                        CreationCollisionOption.ReplaceExisting))
                    {
                        jsonSerializer.WriteObject(stream, resultsDifficult);
                    }
                    break;
                case evil:
                    using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(evilResult,
                        CreationCollisionOption.ReplaceExisting))
                    {
                        jsonSerializer.WriteObject(stream, resultsEvil);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
