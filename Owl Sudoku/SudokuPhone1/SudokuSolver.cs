﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuLibrary
{
    #region Helped classes
    class GridPoint
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public const int gridSize = 9;

        #region constructors
        public GridPoint() : this(0, 0) { }
        public GridPoint(int row, int column)
        {
            Row = row;
            Column = column;
        }
        public GridPoint(int absoluteValue)
        {
            Row = (absoluteValue - absoluteValue % gridSize) / gridSize;
            Column = absoluteValue % gridSize;

        }
        public GridPoint(GridPoint gp)
        {
            this.Column = gp.Column;
            this.Row = gp.Row;
        }
        #endregion

        public static implicit operator GridPoint(int absoluteValue)
        {
            return new GridPoint(absoluteValue);
        }
    }

    public class WrongNumberException : Exception
    {
        public WrongNumberException() { }
        public WrongNumberException(string message) : base(message) { }
        public WrongNumberException(string message, Exception inner) : base(message, inner) { }
        //protected WrongNumberException(
        //  System.Runtime.Serialization.SerializationInfo info,
        //  System.Runtime.Serialization.StreamingContext context)
        //    : base(info, context) { }
    }

    public enum Level
    {
        ExtremelyEasy = 0,
        Easy = 1,
        Medium = 2,
        Difficult = 3,
        Evil = 4
    }

    #endregion

    public class SudokuSolver
    {
        private Random rand;
        public static int GridSize = 9;
        private int[][] sudokuGrid;
        private int[][] outputGrid;
        private int[][] checkingGrid;
        private int[] Line; //line 123456789
        private int[] countOfNumbers; //count of numbers in unfilled sudoku

        public SudokuSolver()
        {
            rand = new Random();
            Line = new int[GridSize];
            countOfNumbers = new int[GridSize];
            sudokuGrid = new int[GridSize][];
            outputGrid = new int[GridSize][];
            checkingGrid = new int[GridSize][];
            for (int i = 0; i < GridSize; i++)
            {
                countOfNumbers[i] = 9;
                Line[i] = i + 1;
                sudokuGrid[i] = new int[GridSize];
                outputGrid[i] = new int[GridSize];
                checkingGrid[i] = new int[GridSize];
            }
            GenerateBaseMatrix();
        }

        #region Generate
        private void GenerateBaseMatrix()
        {
            int line_block = 0; //move for 1 step?
            for (int i = 0; i < GridSize; i++)
            {
                int tmp = (int)Math.Floor((double)i / 3);
                if (tmp > line_block)
                {
                    MoveLine(true, tmp);
                    line_block = tmp;
                }
                else if (i != 0)
                    MoveLine(false, 0);
                for (int j = 0; j < GridSize; j++)
                {
                    sudokuGrid[i][j] = Line[j];
                }
            }
        }

        private void MoveLine(bool move, int times)
        {
            if (move)//One step to left
            {
                for (int i = 0; i < GridSize; i++)
                {
                    Line[i] = i + 1;
                }
                for (int i = 0; i < times; i++)
                {
                    int temp_insert = Line[GridSize - 1]; //variable for inserting into Line
                    int temp_copy = 0; //variable for copying from Line

                    for (int j = GridSize - 1; j >= 0; j--)
                    {
                        if (j != 0)
                        {
                            temp_copy = Line[j - 1];
                            Line[j - 1] = temp_insert;
                            temp_insert = temp_copy;
                        }
                        else if (j == 0)
                        {
                            Line[GridSize - 1] = temp_insert;
                        }
                    }
                }
            }
            else
            {
                //Move line to left 3 times by 1 step
                for (int i = 0; i < 3; i++)
                {
                    int temp_insert = Line[GridSize - 1];
                    int temp_copy = 0;

                    for (int j = GridSize - 1; j >= 0; j--)
                    {
                        if (j != 0)
                        {
                            temp_copy = Line[j - 1];
                            Line[j - 1] = temp_insert;
                            temp_insert = temp_copy;
                        }
                        else if (j == 0)
                        {
                            Line[GridSize - 1] = temp_insert;
                        }
                    }
                }
            }
        }

        private void SwapSmallLine()
        {
            int seekingBlock = rand.Next(0, 3);
            int firstLineNumber = rand.Next(0, 3);
            int secondLineNumber = 0;
            while (firstLineNumber == secondLineNumber)
            {
                secondLineNumber = rand.Next(0, 3);
            }
            for (int i = 0; i < GridSize; i++)
            {
                int tmp = sudokuGrid[seekingBlock * 3 + firstLineNumber][i];
                sudokuGrid[seekingBlock * 3 + firstLineNumber][i] = sudokuGrid[seekingBlock * 3 + secondLineNumber][i];
                sudokuGrid[seekingBlock * 3 + secondLineNumber][i] = tmp;
            }
        }
        private void SwapSmallColumn()
        {
            int seekingBlock = rand.Next(0, 3);
            int firstColumnNumber = rand.Next(0, 3);
            int secondColumnNumber = 0;
            while (firstColumnNumber == secondColumnNumber)
            {
                secondColumnNumber = rand.Next(0, 3);
            }
            for (int i = 0; i < GridSize; i++)
            {
                int tmp = sudokuGrid[i][seekingBlock * 3 + firstColumnNumber];
                sudokuGrid[i][seekingBlock * 3 + firstColumnNumber] = sudokuGrid[i][seekingBlock * 3 + secondColumnNumber];
                sudokuGrid[i][seekingBlock * 3 + secondColumnNumber] = tmp;
            }
        }
        private void SwapLineArea()
        {
            int firstAreaNumber = rand.Next(0, 3);
            int secondAreaNumber = 0;
            while (secondAreaNumber == firstAreaNumber)
                secondAreaNumber = rand.Next(0, 3);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < GridSize; j++)
                {
                    int tmp = sudokuGrid[firstAreaNumber * 3 + i][j];
                    sudokuGrid[firstAreaNumber * 3 + i][j] = sudokuGrid[secondAreaNumber * 3 + i][j];
                    sudokuGrid[secondAreaNumber * 3 + i][j] = tmp;
                }
            }
        }
        private void SwapColumnArea()
        {
            int firstAreaNumber = rand.Next(0, 3);
            int secondAreaNumber = 0;
            while (secondAreaNumber == firstAreaNumber)
                secondAreaNumber = rand.Next(0, 3);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < GridSize; j++)
                {
                    int tmp = sudokuGrid[j][firstAreaNumber * 3 + i];
                    sudokuGrid[j][firstAreaNumber * 3 + i] = sudokuGrid[j][secondAreaNumber * 3 + i];
                    sudokuGrid[j][secondAreaNumber * 3 + i] = tmp;
                }
            }
        }
        private void Transposing()
        {
            for (int i = 0; i < GridSize; i++)
            {
                for (int j = i; j < GridSize; j++)
                {
                    int tmp = sudokuGrid[i][j];
                    sudokuGrid[i][j] = sudokuGrid[j][i];
                    sudokuGrid[j][i] = tmp;
                }
            }
        }
        private void SwapLineNumbers()
        {
            int firstNumber = rand.Next(1, 10);
            int secondNumber = rand.Next(1, 10);
            while (firstNumber == secondNumber)
                secondNumber = rand.Next(1, 10);

            for (int i = 0; i < GridSize; i++)
            {
                int posFirst = -1, posSecond = -1, temp = 0;
                for (int j = 0; j < GridSize; j++)
                {
                    if (sudokuGrid[i][j] == firstNumber)
                        posFirst = j;
                    if (sudokuGrid[i][j] == secondNumber)
                        posSecond = j;
                    if (posFirst >= 0 && posSecond >= 0)
                        break;
                }
                temp = sudokuGrid[i][posFirst];
                sudokuGrid[i][posFirst] = sudokuGrid[i][posSecond];
                sudokuGrid[i][posSecond] = temp;
            }
        }
        private void SwapColumnNumbers()
        {
            int firstNumber = rand.Next(1, 10);
            int secondNumber = rand.Next(1, 10);

            while (firstNumber == secondNumber)
                secondNumber = rand.Next(1, 10);

            for (int i = 0; i < GridSize; i++)
            {
                int posFirst = -1, posSecond = -1, temp = 0;
                for (int j = 0; j < GridSize; j++)
                {
                    if (sudokuGrid[j][i] == firstNumber)
                        posFirst = j;
                    if (sudokuGrid[j][i] == secondNumber)
                        posSecond = j;
                    if (posFirst >= 0 && posSecond >= 0)
                        break;
                }
                temp = sudokuGrid[posFirst][i];
                sudokuGrid[posFirst][i] = sudokuGrid[posSecond][i];
                sudokuGrid[posSecond][i] = temp;
            }
        }

        public async Task GenerateGridAsync()
        {
            await Task.Run(() =>
            {
                int shakeTimes = 100;
                int prevShake = 1;
                for (int i = 0; i < shakeTimes; i++)
                {
                    int funcNumber = rand.Next(1, 6);
                    while (prevShake == funcNumber)
                        funcNumber = rand.Next(1, 8);
                    prevShake = funcNumber;
                    switch (funcNumber)
                    {
                        case 1:
                            SwapSmallLine();
                            break;
                        case 2:
                            SwapSmallColumn();
                            break;
                        case 3:
                            SwapColumnArea();
                            break;
                        case 4:
                            SwapLineArea();
                            break;
                        case 5:
                            Transposing();
                            break;
                        case 6:
                            SwapLineNumbers();
                            break;
                        case 7:
                            SwapColumnNumbers();
                            break;
                        default:
                            break;
                    }
                }
                for (int i = 0; i < GridSize; i++)
                {
                    for (int j = 0; j < GridSize; j++)
                    {
                        outputGrid[i][j] = sudokuGrid[i][j];
                    }
                }
            });
        }
        #endregion

        public async Task CreateConditionAsync(Level lvl)
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < GridSize; i++)
                {
                    countOfNumbers[i] = 9;
                }
                GridPoint prevTemp = new GridPoint(-2, -2);
                GridPoint temp = new GridPoint(-2, -2);
                int levelCount = 0; //count of numbers on field
                int minCount = 0; //minimum count of number
                switch (lvl)
                {
                    case Level.ExtremelyEasy:
                        levelCount = rand.Next(50, 61);
                        minCount = 4;
                        break;
                    case Level.Easy:
                        levelCount = rand.Next(36, 50);
                        minCount = 3;
                        break;
                    case Level.Medium:
                        levelCount = rand.Next(32, 36);
                        minCount = 3;
                        break;
                    case Level.Difficult:
                        levelCount = rand.Next(28, 32);
                        minCount = 3;
                        break;
                    case Level.Evil:
                        levelCount = rand.Next(22, 28);
                        minCount = 2;
                        break;
                    default:
                        break;
                }

                int GridCounter = GridSize * GridSize;

                while (levelCount < GridCounter)
                {
                    GridPoint currentPoint = rand.Next(0, 81);
                    //terrible "if"
                    if (!((Math.Abs(currentPoint.Row - temp.Row) <= 1 && Math.Abs(currentPoint.Column - temp.Column) <= 1) ||
                        (Math.Abs(currentPoint.Row - prevTemp.Row) <= 1 && Math.Abs(currentPoint.Column - prevTemp.Column) <= 1) ||
                        currentPoint.Row == temp.Row || currentPoint.Column == temp.Column ||
                        currentPoint.Row == prevTemp.Row || currentPoint.Column == prevTemp.Column ||
                        outputGrid[currentPoint.Row][currentPoint.Column] == 0))
                    {
                        if (countOfNumbers[sudokuGrid[currentPoint.Row][currentPoint.Column] - 1] > minCount)
                        {
                            countOfNumbers[sudokuGrid[currentPoint.Row][currentPoint.Column] - 1]--;
                            GridCounter--;
                            outputGrid[currentPoint.Row][currentPoint.Column] = 0;
                            prevTemp.Column = temp.Column;
                            prevTemp.Row = temp.Row;
                            temp.Column = currentPoint.Column;
                            temp.Row = currentPoint.Row;
                        }
                    }

                }

                for (int i = 0; i < GridSize; i++)
                {
                    for (int j = 0; j < GridSize; j++)
                    {
                        checkingGrid[i][j] = outputGrid[i][j];
                    }
                }
            });
        }

        public int GetPoint(int row, int column)
        {
            if (row < 9 && row >= 0 && column >= 0 && column < 9)
                return outputGrid[row][column];
            else
                return -1;
        }

        public async Task<bool> CheckingAsync(int row, int column, int value)
        {
            return await Task.Run(() =>
            {
                if (value > 9 || value < 1)
                    return false;
                if (row < 0 || column < 0)
                    return false;
                bool checkRow = true, checkColumn = true, checkBlock = true;
                //check in row and column
                for (int i = 0; i < GridSize; i++)
                {
                    if (column != i)
                    {
                        if (checkingGrid[row][i] == value)
                        {
                            checkRow = false;
                            break;
                        }
                    }
                    if (row != i)
                    {
                        if (checkingGrid[i][column] == value)
                        {
                            checkColumn = false;
                            break;
                        }
                    }
                }
                //Check in block
                int floorRow = Convert.ToInt32(Math.Floor((double)(row / 3))) * 3;
                int upRow = floorRow + 3;
                int floorColumn = Convert.ToInt32(Math.Floor((double)(column / 3))) * 3;
                int upColumn = floorColumn + 3;
                for (int i = floorRow; i < upRow; i++)
                {
                    for (int j = floorColumn; j < upColumn; j++)
                    {
                        if (i != row && j != column)
                        {
                            if (checkingGrid[i][j] == value)
                            {
                                checkBlock = false;
                                break;
                            }
                        }
                    }
                    if (!checkBlock)
                        break;
                }

                //result of checking
                if (checkRow && checkColumn && checkBlock)
                {
                    checkingGrid[row][column] = value;
                    return true;
                }
                else
                    return false;
            });
        }

        public async Task ClearAsync()
        {
            await Task.Run(() =>
                {
                    for (int i = 0; i < GridSize; i++)
                    {
                        for (int j = 0; j < GridSize; j++)
                        {
                            checkingGrid[i][j] = outputGrid[i][j];
                        }
                    }
                });
        }
    }
}