﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SudokuLibrary;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading;
using System.Globalization;
using Windows.Graphics.Display;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SudokuPhone1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlayPage : Page
    {
        private Level level;
        private SudokuSolver solver;
        private short countOfNumbers=0;
        short selectedRow = -1, selectedColumn = -1;
        private TextBlock[][] sudokuBlocks;
        private TextBlock[] inputBlocks;
        private Image[][] imageOnBlocks;
        private Image[] imageOnInput;
        private Rectangle[][] rectForSudoku;
        private Rectangle[] rectForInput;
        private DateTime startTime = DateTime.Now;
        private TimeSpan currentTime;
        private DispatcherTimer timer;
        

        public PlayPage()
        {
            this.InitializeComponent();
            solver = new SudokuSolver();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Tick+=timer_Tick;
            timer.Start();
            //timer.Interval = 1000;
            string parameter = e.Parameter.ToString();
            switch(parameter)
            {
                case "ExtEasy":
                    level = Level.ExtremelyEasy;
                    break;
                case "Easy":
                    level = Level.Easy;
                    break;
                case "Medium":
                    level = Level.Medium;
                    break;
                case "Difficult":
                    level = Level.Difficult;
                    break;
                case "Evil":
                    level = Level.Evil;
                    break;
                default:
                    break;
            }
            await solver.GenerateGridAsync();
            await solver.CreateConditionAsync(level);
            Grid gr = new Grid();
            SetToGrid();
            InputData();

        }

        private void timer_Tick(object sender, object e)
        {
            TimeSpan plus = new TimeSpan(0, 0, 5);
            currentTime = DateTime.Now - startTime;
            timeBlock.FontSize = 50;
            timeBlock.Foreground = new SolidColorBrush(Colors.Black);
            timeBlock.Text = String.Format("{0:hh}:{1:mm}:{2:ss}",currentTime,currentTime,currentTime);
            timeBlock.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            timeBlock.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            timeBlock.TextAlignment = TextAlignment.Center;
        }

        private void SetToGrid()
        {
            sudokuBlocks = new TextBlock[SudokuLibrary.SudokuSolver.GridSize][];
            rectForSudoku = new Rectangle[SudokuLibrary.SudokuSolver.GridSize][];
            imageOnBlocks = new Image[SudokuLibrary.SudokuSolver.GridSize][];
            inputBlocks = new TextBlock[SudokuLibrary.SudokuSolver.GridSize];
            rectForInput = new Rectangle[SudokuLibrary.SudokuSolver.GridSize];
            imageOnInput = new Image[SudokuLibrary.SudokuSolver.GridSize];
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Color.FromArgb(200, 197, 246, 240);

            for (int i = 0; i < SudokuLibrary.SudokuSolver.GridSize; i++)
            {
                sudokuBlocks[i] = new TextBlock[SudokuLibrary.SudokuSolver.GridSize];
                rectForSudoku[i] = new Rectangle[SudokuLibrary.SudokuSolver.GridSize];
                imageOnBlocks[i] = new Image[SudokuLibrary.SudokuSolver.GridSize];
                for (int j = 0; j < SudokuLibrary.SudokuSolver.GridSize; j++)
                {
                    //initialize blocks
                    sudokuBlocks[i][j] = new TextBlock();
                    sudokuBlocks[i][j].Name = String.Format("sudoku{0}{1}", i, j);
                    sudokuBlocks[i][j].FontSize = 30;
                    sudokuBlocks[i][j].HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
                    sudokuBlocks[i][j].VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
                    sudokuBlocks[i][j].TextWrapping = TextWrapping.NoWrap;
                    sudokuBlocks[i][j].TextAlignment = TextAlignment.Center;
                    sudokuBlocks[i][j].Height = 40;
                    sudokuBlocks[i][j].Width = 40;
                    sudokuBlocks[i][j].Foreground = new SolidColorBrush(Colors.Black);
                    sudokuBlocks[i][j].Tapped += sudokuBlock_Tapped;
                    //initialize rectangles
                    rectForSudoku[i][j] = new Rectangle();
                    rectForSudoku[i][j].Name = String.Format("Rect{0}{1}", i, j);
                    rectForSudoku[i][j].Fill = scb;
                    rectForSudoku[i][j].Height = 40;
                    rectForSudoku[i][j].Width = 40;
                    //initialize images
                    imageOnBlocks[i][j] = new Image();
                    imageOnBlocks[i][j].Height = 40;
                    imageOnBlocks[i][j].Width = 40;
                    imageOnBlocks[i][j].Stretch = Stretch.Fill;
                    if (i % 3 == 0 && j % 3 == 0)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/LeftUp.png"));
                    else if (i % 3 == 0 && j % 3 == 1)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Up.png"));
                    else if (i % 3 == 0 && j % 3 == 2)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/RightUp.png"));
                    else if (i % 3 == 1 && j % 3 == 0)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Left.png"));
                    else if (i % 3 == 1 && j % 3 == 1)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Free.png"));
                    else if (i % 3 == 1 && j % 3 == 2)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Right.png"));
                    else if (i % 3 == 2 && j % 3 == 0)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/LeftDown.png"));
                    else if (i % 3 == 2 && j % 3 == 1)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Bottom.png"));
                    else if (i % 3 == 2 && j % 3 == 2)
                        imageOnBlocks[i][j].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/RightDown.png"));

                    sudokuGrid.Children.Add(rectForSudoku[i][j]);
                    Grid.SetRow(rectForSudoku[i][j], i);
                    Grid.SetColumn(rectForSudoku[i][j], j);
                    sudokuGrid.Children.Add(imageOnBlocks[i][j]);
                    Grid.SetRow(imageOnBlocks[i][j], i);
                    Grid.SetColumn(imageOnBlocks[i][j], j);
                    sudokuGrid.Children.Add(sudokuBlocks[i][j]);
                    Grid.SetRow(sudokuBlocks[i][j], i);
                    Grid.SetColumn(sudokuBlocks[i][j], j);
                }

                inputBlocks[i] = new TextBlock();
                inputBlocks[i].Name = String.Format("input{0}", i);
                inputBlocks[i].Text = String.Format("{0}", i + 1);
                inputBlocks[i].Tag = String.Format("{0}", i + 1);
                inputBlocks[i].FontSize = 30;
                inputBlocks[i].HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
                inputBlocks[i].VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
                inputBlocks[i].TextWrapping = TextWrapping.NoWrap;
                inputBlocks[i].TextAlignment = TextAlignment.Center;
                inputBlocks[i].Height = 40;
                inputBlocks[i].Width = 40;
                inputBlocks[i].Foreground = new SolidColorBrush(Colors.Black);
                inputBlocks[i].Tapped += inputBlock_Tapped;


                rectForInput[i] = new Rectangle();
                rectForInput[i].Name = String.Format("Rect{0}", i);
                rectForInput[i].Fill = scb;
                rectForInput[i].Height = 40;
                rectForInput[i].Width = 40;

                imageOnInput[i] = new Image();
                imageOnInput[i].Height = 40;
                imageOnInput[i].Width = 40;
                imageOnInput[i].Stretch = Stretch.Fill;
                imageOnInput[i].Source = new BitmapImage(new Uri(this.BaseUri, "Assets/borders/Free.png"));

                inputGrid.Children.Add(rectForInput[i]);
                Grid.SetColumn(rectForInput[i], i);
                inputGrid.Children.Add(imageOnInput[i]);
                Grid.SetColumn(imageOnInput[i], i);
                inputGrid.Children.Add(inputBlocks[i]);
                Grid.SetColumn(inputBlocks[i], i);

            }
        }

        private async void inputBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            int value = Convert.ToInt32(((TextBlock)sender).Tag);
            bool result = await solver.CheckingAsync(selectedRow, selectedColumn, value);
            if (result)
            {
                sudokuBlocks[selectedRow][selectedColumn].Text = String.Format("{0}", value);
                SolidColorBrush scb = new SolidColorBrush();
                scb.Color = Color.FromArgb(200, 197, 246, 240);
                rectForSudoku[selectedRow][selectedColumn].Fill = scb;
                selectedRow = -1;
                selectedColumn = -1;
                countOfNumbers++;
                CheckCount();
            }
            else
            {
                startTime -= new TimeSpan(0, 0, 5);
            }
            
        }

        private void InputData()
        {
            for (int i = 0; i < SudokuSolver.GridSize; i++)
            {
                for (int j = 0; j < SudokuSolver.GridSize; j++)
                {
                    sudokuBlocks[i][j].IsTapEnabled = true;
                    if (solver.GetPoint(i, j) != 0)
                    {
                        sudokuBlocks[i][j].Text = string.Format("{0}", solver.GetPoint(i, j));
                        sudokuBlocks[i][j].IsTapEnabled = false;
                        countOfNumbers++;
                    }
                    else
                        sudokuBlocks[i][j].Text = "";
                }
            }
        }

        private void sudokuBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Color.FromArgb(200, 197, 246, 240);
            if (selectedRow >= 0 && selectedColumn >= 0)
                rectForSudoku[selectedRow][selectedColumn].Fill = scb;
            TextBlock tempTextBlock = (TextBlock)sender;
            selectedRow = Convert.ToInt16(tempTextBlock.Name.Substring(6, 1));
            selectedColumn = Convert.ToInt16(tempTextBlock.Name.Substring(7, 1));
            rectForSudoku[selectedRow][selectedColumn].Fill = new SolidColorBrush(Colors.Red);
        }

        private void backbtn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(LevelPage));
        }

        private async void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            if (selectedColumn < 0 || selectedRow < 0)
                return;
            sudokuBlocks[selectedRow][selectedColumn].Text = "";
            bool result = await solver.CheckingAsync(selectedRow, selectedColumn, 0);
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Color.FromArgb(200, 197, 246, 240);
            rectForSudoku[selectedRow][selectedColumn].Fill = scb;
            selectedRow = -1;
            selectedColumn = -1;
            countOfNumbers--;
        }

        private void CheckCount()
        {
            if (countOfNumbers==81)
            {
                Frame.Navigate(typeof(WinPage), currentTime);
            //    MessageDialog messageDialog = new MessageDialog("Congratulations!!!!");
            //    messageDialog.Commands.Add(new UICommand(
            //        "Done!",
            //        new UICommandInvokedHandler(this.CommandInvokedHandler)));

            //    messageDialog.DefaultCommandIndex = 0;
            //    messageDialog.CancelCommandIndex = 1;

            //    await messageDialog.ShowAsync();
            }
        }

        private void CommandInvokedHandler(IUICommand command)
        {
            if (command.Label == "Done!")
                Frame.Navigate(typeof(MainPage));
        }

        private void imgBack_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(LevelPage));
        }

        private async void imgDelete_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (selectedColumn < 0 || selectedRow < 0)
                return;
            sudokuBlocks[selectedRow][selectedColumn].Text = "";
            bool result = await solver.CheckingAsync(selectedRow, selectedColumn, 0);
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Color.FromArgb(200, 197, 246, 240);
            rectForSudoku[selectedRow][selectedColumn].Fill = scb;
            selectedRow = -1;
            selectedColumn = -1;
            countOfNumbers--;
        }

        private async void imgClear_Tapped(object sender, TappedRoutedEventArgs e)
        {
            await solver.ClearAsync();
            InputData();
        }
    }
}
